# AUTOMATION PRACTISE SITE
-------------------

## Description
-------------------
This is a framework for selenium tests with defined some test cases. Every test cases are for website http://automationpractice.com/index.php.
I have used for this project Cucumber, testNG and page object pattern for web elements. 
Test cases are running by cucumber with testNG or with only testNG.
Framework is compatible with local web drivers and selenium grid.

Below you can see what test cases are right now in this test script.

TC for Cucumber:
> - User doesn't enter correct email (Customer service form)
> - User try to send message with correct email but without text (Customer service form)
> - User try to send message with correct email and text but without choosing subject heading (Customer service form)
> - Positive scenario about send message to customer service (Customer service form)
> - Positive scenario about login to application but email is incorrect (Fail Scenarios For Screenshot Mechanism)
> - Positive scenario about login to application (Login)
> - User doesn't enter email (Login)
> - User enter wrong email format (Login)
> - User enter correct email but without password (Login)
> - User enter correct email but without password (Login)
> - User enter correct email but with wrong password (Login)
> - User go to login Page (Navigation)
> - User go to contact us page (Navigation)
> - User go to forget your password page (Navigation)

TC for testNG:
> - As user go to customer service site and send message (Customer service scenarios)
> - As user log in to application and log out (Fail Scenarios For Screenshot Mechanism)
> - As user log in to application and log out (Login and log out scenarios)
> - As user add to cart first popular product and check this one in cart (Adding popular products to cart scenarios)
> - As user add to cart all popular products and check these ones in cart (Adding popular products to cart scenarios)
> - As user add to cart first best seller product and check this one in cart (Adding best sellers products to cart scenarios)
> - As user add to cart all best sellers products and check these ones in cart (Adding best sellers products to cart scenarios)

Framework has following features:
> - Class with assertions AssertWebElement which use assertj library
> - Class with listeners DriverEventListener for events during execution time of tests 
> - Class BrowserFactory where you can add browsers for tests. For now there are browsers like (Chrome, Firefox, Opera, IE)
> - Mechanism to do screenshots in class screenShotMaker
> - Mechanism to retry scenario in class RetryAnalyzer when status of test case is fail 
> - Class with listeners TestListener where you can define actions before and after execution of test case
> - Class with waits WaitForElement
> - XML file for running test cucumber and testNG scenarios
> - Configuration file configuration.properties where you can specify url to testing website, location on your computer webdrivers...
> - Class for JavaScript scripts
> - Class for keyboard and mouse actions

## Installation
-------------------
Installation is very simple, every thing which you need to do is download this code and type in cmd command.
> mvn clean install

## Run of application
-------------------
After installation, type in cmd command.
> mvn clean test

## Report
-------------------
I used allure library for report generation and report is saving in location target/site/allure-maven-plugin
For generate report you need write mvn allure:report command in cmd.

# Web Drivers
-------------------
I putted web drivers to location src/test/java/utils/drivers. 
Make sure that you have proper version of browsers.

Below you can see versions of web drivers which i use.
> - ChromeDriver 87.0.4280.88

# Enjoy yourself!!!!!!!