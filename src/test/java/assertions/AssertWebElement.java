package assertions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.AbstractAssert;
import org.openqa.selenium.WebElement;

/**
 * Class with assertions for web elements.
 */
public class AssertWebElement extends AbstractAssert<AssertWebElement, WebElement> {

    private Logger logger = LogManager.getLogger(AssertWebElement.class);

    public AssertWebElement(WebElement webElement) {
        super(webElement, AssertWebElement.class);
    }

    public static AssertWebElement assertThat(WebElement webElement) {
        return new AssertWebElement(webElement);
    }

    /**
     * Method for checking visibility of web element on page.
     *
     * @return method return asserted web element.
     */
    public AssertWebElement isDisplayed() {
        logger.info("Checking if element is displayed");
        isNotNull();

        if (!actual.isDisplayed()) {
            failWithMessage("Element was not displayed!");
        }
        logger.info("WebElement was displayed!");
        return this;
    }

    /**
     * Method for checking text in web element.
     * @param expectedTextValue expected text in element.
     * @return method return asserted web element.
     */
    public AssertWebElement hasText(String expectedTextValue) {
        logger.info("Checking if WebElement has text: " + expectedTextValue);
        isNotNull();

        String actualElementText = actual.getText();
        if (!actualElementText.equals(expectedTextValue)) {
            failWithMessage("Element text was <%s> expecting to be <%s>!", actualElementText, expectedTextValue);
        }

        logger.info("WebElement had expected text!");
        return this;
    }

}
