package cucumber.tests;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.manager.DriverUtils;
import pageobjects.pages.actions.LoginPageActions;
import pageobjects.pages.actions.MyAccountPageActions;

import static navigation.ApplicationUrls.SING_IN_URL;
import static pageobjects.pages.actions.LoginPageActions.getLoginPageActions;
import static pageobjects.pages.actions.MyAccountPageActions.getMyAccountPageActions;

/**
 * Class with mapped steps from feature file Login for test cases
 */
public class LoginSteps {

    private LoginPageActions loginPageActions = getLoginPageActions();
    private MyAccountPageActions myAccountPageElementsActions = getMyAccountPageActions();

    @Given("^User is on Login page$")
    public void user_is_on_Login_page() {
        DriverUtils.navigateToPage(SING_IN_URL);
    }

    @When("^User login into application with correct email \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void user_login_into_application_with_correct_email_and_password(String email, String password) {
        loginActions(email, password);
    }

    @Then("^User logged to application and got proper information \"([^\"]*)\"$")
    public void user_logged_to_application_and_got_proper_information(String successMessage) {
        myAccountPageElementsActions.assertThatInfoAccountIsDisplayed(successMessage);
    }

    @When("^User login into application without email \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void user_login_into_application_without_email_and_password(String email, String password) {
        loginActions(email, password);
    }

    @When("^User login into application with wrong email format \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void user_login_into_application_with_wrong_email_format_and_password(String email, String password) {
        loginActions(email, password);
    }

    @Then("^User got error \"([^\"]*)\"$")
    public void user_got_error(String errorMessage) {
        loginPageActions.assertThatLoginErrorIsDisplayed(errorMessage);
    }

    @When("^User login into application with correct email \"([^\"]*)\" but without password \"([^\"]*)\"$")
    public void user_login_into_application_with_correct_email_but_without_password(String email, String password) {
        loginActions(email, password);
    }

    @When("^User login into application with correct email \"([^\"]*)\" but with wrong password \"([^\"]*)\"$")
    public void user_login_into_application_with_correct_email_but_with_wrong_password(String email, String password) {
        loginActions(email, password);
    }

    private void loginActions(String email, String password) {
        loginPageActions.fillInEmailInput(email)
                .fillInPasswordInput(password)
                .clickOnButtonSingIn();
    }

}
