package cucumber.tests;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.manager.DriverUtils;
import pageobjects.pages.actions.CustomerServicePageActions;

import java.io.File;

import static navigation.ApplicationUrls.CUSTOMER_SERVICE_URL;
import static pageobjects.pages.actions.CustomerServicePageActions.getCustomerServicePageActions;

/**
 * Class with mapped steps from feature file CustomerServiceForm for test cases
 */
public class CustomerServiceSteps {

    private CustomerServicePageActions customerServicePageActions = getCustomerServicePageActions();

    @Given("^User is on contact us site$")
    public void user_is_on_contact_us_site() {
        DriverUtils.navigateToPage(CUSTOMER_SERVICE_URL);
    }

    @When("^User choose subject heading (.+)$")
    public void user_choose_subject_heading(String subjectHeading) {
        customerServicePageActions.chooseSubjectHeading(subjectHeading);
    }

    @When("^User fill in email address input with email \"([^\"]*)\"$")
    public void user_fill_in_email_address_input_with_email(String email) {
        customerServicePageActions.fillInEmailAddressInput(email);
    }

    @When("^User fill in order reference input with data (.+)$")
    public void user_fill_in_order_reference_input_with_data(String orderReference) {
        customerServicePageActions.fillInOrderReferenceInput(orderReference);
    }

    @When("^User attach file from directory \"([^\"]*)\"$")
    public void user_attach_file_from_directory(String fileDirectory) {
        customerServicePageActions.attachFile(new File(fileDirectory).getAbsolutePath());
    }

    @When("^User fill in text field with text \"([^\"]*)\"$")
    public void user_fill_in_text_field_with_text(String text) {
        customerServicePageActions.fillInTextField(text);
    }

    @When("^User click button Send$")
    public void user_click_button_Send() {
        customerServicePageActions.clickOnButtonSend();
    }

    @Then("^User got proper message \"([^\"]*)\"$")
    public void user_got_proper_message(String message) {
        customerServicePageActions.assertThatContactServiceSuccessMessageIsCorrect(message);
    }

    @When("^User fill in email address input with wrong email format \"([^\"]*)\"$")
    public void user_fill_in_email_address_input_with_wrong_email_format(String email) {
        customerServicePageActions.fillInEmailAddressInput(email);
    }

    @Then("^User got error message \"([^\"]*)\"$")
    public void user_got_error_message(String error) {
        customerServicePageActions.assertThatContactServiceErrorIsCorrect(error);
    }

}
