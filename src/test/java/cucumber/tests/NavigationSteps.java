package cucumber.tests;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.manager.DriverUtils;
import io.cucumber.datatable.DataTable;
import pageobjects.pages.actions.CustomerServicePageActions;
import pageobjects.pages.actions.ForgotYourPasswordPageActions;
import pageobjects.pages.actions.HeaderPageActions;
import pageobjects.pages.actions.LoginPageActions;

import java.util.List;

import static navigation.ApplicationUrls.MAIN_URL;
import static navigation.ApplicationUrls.SING_IN_URL;
import static pageobjects.pages.actions.CustomerServicePageActions.getCustomerServicePageActions;
import static pageobjects.pages.actions.ForgotYourPasswordPageActions.getForgotYourPasswordPageActions;
import static pageobjects.pages.actions.HeaderPageActions.getHeaderPageActions;
import static pageobjects.pages.actions.LoginPageActions.getLoginPageActions;

/**
 * Class with mapped steps from feature file NavigationSteps for test cases.
 */
public class NavigationSteps {

    HeaderPageActions headerPageActions = getHeaderPageActions();
    LoginPageActions loginPageActions = getLoginPageActions();
    CustomerServicePageActions customerServicePageActions = getCustomerServicePageActions();
    ForgotYourPasswordPageActions forgotYourPasswordPageActions = getForgotYourPasswordPageActions();

    @Given("^User is on main page$")
    public void user_is_on_main_page() {
        DriverUtils.navigateToPage(MAIN_URL);
    }

    @Given("^User is on login page$")
    public void user_is_on_login_page() {
        DriverUtils.navigateToPage(SING_IN_URL);
    }

    @When("^User click on button Sing in$")
    public void user_click_on_button_Sing_in() {
        headerPageActions.clickOnSingInButton();
    }

    @Then("^User is on login page and correct headings are displayed$")
    public void user_is_on_login_page_and_correct_heading_are_displayed(DataTable dataTable) {
        List<String> list = dataTable.asList();
        loginPageActions.assertThatHeadingsOnLoginPageAreDisplayed(list.get(0), list.get(1), list.get(2));
    }

    @When("^User click on button contact us$")
    public void user_click_on_button_contact_us() {
        headerPageActions.clickOnContactButton();
    }

    @Then("^User is on contact us page and heading \"([^\"]*)\" is displayed$")
    public void user_is_on_contact_us_page_and_heading_is_displayed(String heading) {
        customerServicePageActions.assertThatHeadingCustomerServiceIsDisplayed(heading);
    }

    @When("^User click on link forget your password$")
    public void user_click_on_link_forget_your_password() {
        loginPageActions.clickOnLinkForgetPassword();
    }

    @Then("^User is on forget your password page and heading \"([^\"]*)\" is displayed$")
    public void user_is_on_forget_your_password_page_and_heading_is_displayed(String heading) {
        forgotYourPasswordPageActions.assertThatInfoAccountIsDisplayed(heading);
    }

}
