Feature: Fail scenarios about check if screenshot mechanism work properly

Scenario: Positive scenario about login to application but email is incorrect
Given User is on Login page
When User login into application with correct email "etestautomatt@gmail.com" and password "ADMIN"
Then User logged to application and got proper information "Welcome to your account. Here you can manage all of your personal information and orders."