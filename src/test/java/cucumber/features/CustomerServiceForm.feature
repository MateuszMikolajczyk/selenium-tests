Feature: Checking the correct operation of the customer service form

Scenario: User doesn't enter correct email (Negative scenario)
Given User is on contact us site
When User fill in email address input with wrong email format "testautomatt@gmail"
And User click button Send
Then User got error message "Invalid email address."

Scenario: User try send message with correct email but without text (Negative scenario)
Given User is on contact us site
When User fill in email address input with email "testautomatt@gmail.com"
And User click button Send
Then User got error message "The message cannot be blank."

Scenario: User try send message with correct email and text but without choosing subject heading (Negative scenario)
Given User is on contact us site
When User fill in email address input with email "testautomatt@gmail.com"
And User fill in text field with text "Test"
And User click button Send
Then User got error message "Please select a subject from the list provided."

Scenario Outline: Positive scenario about send message to customer service
Given User is on contact us site
When User fill in email address input with email "testautomatt@gmail.com"
And User choose subject heading <Subject heading>
And User fill in order reference input with data <Order reference>
And User attach file from directory "src/test/resources/TestFile.txt"
And User fill in text field with text "Test"
And User click button Send
Then User got proper message "Your message has been successfully sent to our team."

Examples:
|Subject heading |Order reference|
|Customer service|123            |
|Webmaster       |321            |