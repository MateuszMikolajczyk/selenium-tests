Feature: Application Login

Scenario: Positive scenario about login to application
Given User is on Login page
When User login into application with correct email "testautomatt@gmail.com" and password "ADMIN"
Then User logged to application and got proper information "Welcome to your account. Here you can manage all of your personal information and orders."

Scenario: User doesn't enter email (Negative scenario)
Given User is on Login page
When User login into application without email "" and password ""
Then User got error "An email address required."

Scenario: User enter wrong email format (Negative scenario)
Given User is on Login page
When User login into application with wrong email format "testautomattgmail.com" and password ""
Then User got error "Invalid email address."

Scenario: User enter correct email but without password (Negative scenario)
Given User is on Login page
When User login into application with correct email "testautomatt@gmail.com" but without password ""
Then User got error "Password is required."

Scenario: User enter correct email but with wrong password (Negative scenario)
Given User is on Login page
When User login into application with correct email "testautomatt@gmail.com" but with wrong password "x"
Then User got error "Invalid password."

