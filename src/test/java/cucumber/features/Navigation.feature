Feature: Navigation of Application

Scenario: User go to login Page
Given User is on main page
When User click on button Sing in
Then User is on login page and correct headings are displayed
|AUTHENTICATION     |
|CREATE AN ACCOUNT  |
|ALREADY REGISTERED?|

Scenario: User go to contact us page
Given User is on main page
When User click on button contact us
Then User is on contact us page and heading "CUSTOMER SERVICE - CONTACT US" is displayed

Scenario: User go to forget your password page
Given User is on login page
When User click on link forget your password
Then User is on forget your password page and heading "FORGOT YOUR PASSWORD?" is displayed