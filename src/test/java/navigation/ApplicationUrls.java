package navigation;

import configuration.AppProperties;

/**
 * Class with defined URLs for tests.
 */
public class ApplicationUrls {
    public static final String MAIN_URL = AppProperties.getMainUrl();
    public static final String SING_IN_URL = AppProperties.getMainUrl() + AppProperties.getLoginUrl();
    public static final String CUSTOMER_SERVICE_URL = AppProperties.getMainUrl() + AppProperties.getServiceCustomerUrl();
}
