package pageobjects.modalswindows.elements;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

public class AddToCartModalWindowElements extends BasePage {

    @FindBy(id = "layer_cart_product_title")
    private WebElement nameOfProduct;

    @FindBy(id = "layer_cart_product_attributes")
    private WebElement detailsOfProduct;

    @FindBy(id = "layer_cart_product_quantity")
    private WebElement quantityOfProduct;

    @FindBy(id = "layer_cart_product_price")
    private WebElement priceOfProduct;

    @FindBy(xpath = "//div[@id='layer_cart']/*//div[1]/h2")
    private WebElement informationAboutThatProductWasAddedToCart;

    @FindBy(xpath = "//div[@id='layer_cart']/*//div[2]/h2")
    private WebElement informationAboutHowManyProductsAreInCart;

    @FindBy(xpath = "//div[@class='button-container']/span/span")
    private WebElement continueShoppingButton;

    @FindBy(xpath = "//div[@class='button-container']/a")
    private WebElement proceedToCheckoutButton;

    @FindBy(className = "ajax_block_products_total")
    private WebElement totalProductsPrice;

    @FindBy(xpath = "(//div[@class='layer_cart_row'])[2]/span")
    private WebElement shippingPrice;

    @FindBy(xpath = "(//div[@class='layer_cart_row'])[3]/span")
    private WebElement totalPriceWithShipping;

    public WebElement getNameOfProduct() {
        return nameOfProduct;
    }

    public WebElement getDetailsOfProduct() {
        return detailsOfProduct;
    }

    public WebElement getQuantityOfProducts() {
        return quantityOfProduct;
    }

    public WebElement getPriceOfProduct() {
        return priceOfProduct;
    }

    public WebElement getInformationAboutThatProductWasAddedToCart() {
        return informationAboutThatProductWasAddedToCart;
    }

    public WebElement getInformationAboutHowManyProductsAreInCart() {
        return informationAboutHowManyProductsAreInCart;
    }

    public WebElement getContinueShoppingButton() {
        return continueShoppingButton;
    }

    public WebElement getShippingPrice() {
        return shippingPrice;
    }

    public WebElement getTotalPriceWithShipping() {
        return totalPriceWithShipping;
    }

    public Logger getLogger() {
        return logger;
    }

}
