package pageobjects.modalswindows.elements;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

import java.util.List;

public class CartAsDropDownMenuModalWindowElements extends BasePage {

    @FindBy(className = "cart_block_product_name")
    private List<WebElement> nameOfProduct;

    @FindBy(xpath = "//span[@class='quantity-formated']/span")
    private List<WebElement> quantityOfProducts;

    @FindBy(xpath = "//div[@class='product-atributes']/a")
    private List<WebElement> productDetail;

    @FindBy(xpath = "//span[@class='price']")
    private List<WebElement> priceOfProduct;

    @FindBy(xpath = "//div[@class='cart-prices-line first-line']/span[1]")
    private WebElement shippingPrice;

    @FindBy(xpath = "//div[@class='cart-prices-line last-line']/span[1]")
    private WebElement totalPriceWithShipping;

    public List<WebElement> getNameOfProduct() {
        return nameOfProduct;
    }

    public List<WebElement> getQuantityOfProducts() {
        return quantityOfProducts;
    }

    public List<WebElement> getProductDetail() {
        return productDetail;
    }

    public List<WebElement> getPriceOfProduct() {
        return priceOfProduct;
    }

    public WebElement getShippingPrice() {
        return shippingPrice;
    }

    public WebElement getTotalPriceWithShipping() {
        return totalPriceWithShipping;
    }

    public Logger getLogger() {
        return logger;
    }

}
