package pageobjects.modalswindows.actions;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import pageobjects.modalswindows.elements.AddToCartModalWindowElements;
import pageobjects.pages.actions.HomePageActions;
import waits.WaitForElement;

public class AddToCartModalWindowActions {

    private AddToCartModalWindowElements addToCartModalWindowElements;

    private AddToCartModalWindowActions() {
        addToCartModalWindowElements = new AddToCartModalWindowElements();
    }

    public static AddToCartModalWindowActions getAddToCartModalWindowActions() {
        return new AddToCartModalWindowActions();
    }

    @Step("Checking if information is displaying '{information}' in modal window after user added product to cart.")
    public AddToCartModalWindowActions checkInformationAboutThatProductWasAddedToCart(String information) {
        addToCartModalWindowElements.getLogger().info("Checking which information is displaying in modal window");
        WaitForElement.waitUntilElementIsVisible(addToCartModalWindowElements.getInformationAboutThatProductWasAddedToCart());
        AssertWebElement.assertThat(addToCartModalWindowElements.getInformationAboutThatProductWasAddedToCart()).hasText(information);
        addToCartModalWindowElements.getLogger().info("Information in modal window is: " + information);
        return this;
    }

    @Step("Checking if information is displaying '{information}' in modal window after user added product to cart.")
    public AddToCartModalWindowActions checkInformationAboutThatHowManyProductsAreInCart(String information) {
        addToCartModalWindowElements.getLogger().info("Checking which information is displaying in modal window");
        WaitForElement.waitUntilElementIsVisible(addToCartModalWindowElements.getInformationAboutHowManyProductsAreInCart());
        AssertWebElement.assertThat(addToCartModalWindowElements.getInformationAboutHowManyProductsAreInCart()).hasText(information);
        addToCartModalWindowElements.getLogger().info("Information in modal window is: " + information);
        return this;
    }

    @Step("User click button 'Continue Shopping' in modal window.")
    public HomePageActions clickButtonContinueShopping() {
        addToCartModalWindowElements.getLogger().info("Trying to click button 'Continue Shopping'");
        WaitForElement.waitUntilElementIsVisible(addToCartModalWindowElements.getContinueShoppingButton());
        addToCartModalWindowElements.getContinueShoppingButton().click();
        addToCartModalWindowElements.getLogger().info("Button 'Continue Shopping' is clicked");
        return HomePageActions.getHomePageActions();
    }


    public String gatherNameOfProduct() {
        WaitForElement.waitUntilElementIsVisible(addToCartModalWindowElements.getNameOfProduct());
        return addToCartModalWindowElements.getNameOfProduct().getText();
    }

    public String gatherQuantityOfProducts() {
        WaitForElement.waitUntilElementIsVisible(addToCartModalWindowElements.getQuantityOfProducts());
        return addToCartModalWindowElements.getQuantityOfProducts().getText();
    }

    public String gatherDetailsOfProduct() {
        WaitForElement.waitUntilElementIsVisible(addToCartModalWindowElements.getDetailsOfProduct());
        return addToCartModalWindowElements.getDetailsOfProduct().getText();
    }

    public String gatherPriceOfProduct() {
        WaitForElement.waitUntilElementIsVisible(addToCartModalWindowElements.getPriceOfProduct());
        return addToCartModalWindowElements.getPriceOfProduct().getText();
    }

    public String gatherShippingPrice() {
        WaitForElement.waitUntilElementIsVisible(addToCartModalWindowElements.getShippingPrice());
        return addToCartModalWindowElements.getShippingPrice().getText();
    }

    public String gatherTotalPriceWithShipping() {
        WaitForElement.waitUntilElementIsVisible(addToCartModalWindowElements.getTotalPriceWithShipping());
        return addToCartModalWindowElements.getTotalPriceWithShipping().getText();
    }
}
