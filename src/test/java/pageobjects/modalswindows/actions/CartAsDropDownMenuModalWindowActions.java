package pageobjects.modalswindows.actions;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import pageobjects.modalswindows.elements.CartAsDropDownMenuModalWindowElements;
import utils.ActionsForCursorAndKeyboard;
import utils.JavascriptForTests;
import waits.WaitForElement;

public class CartAsDropDownMenuModalWindowActions {

    private CartAsDropDownMenuModalWindowElements cartAsDropDownMenuModalWindowElements;
    private JavascriptForTests javascriptForTests;
    private ActionsForCursorAndKeyboard actionsForCursorAndKeyboard;

    private CartAsDropDownMenuModalWindowActions() {
        cartAsDropDownMenuModalWindowElements = new CartAsDropDownMenuModalWindowElements();
        javascriptForTests = new JavascriptForTests();
        actionsForCursorAndKeyboard = new ActionsForCursorAndKeyboard();
    }

    public static CartAsDropDownMenuModalWindowActions getCartAsDropDownMenuModalWindowActions() {
        return new CartAsDropDownMenuModalWindowActions();
    }

    @Step("Checking amount of products in cart. Amount of products should be equals {quantityOfProducts}")
    public CartAsDropDownMenuModalWindowActions checkQuantityOfProductsInCart(String quantityOfProducts, int numberOfProduct) {
        cartAsDropDownMenuModalWindowElements.getLogger().info("Checking amount of products in cart.");
        WaitForElement.waitUntilElementIsVisible(cartAsDropDownMenuModalWindowElements.getQuantityOfProducts().get(numberOfProduct - 1));
        AssertWebElement.assertThat(cartAsDropDownMenuModalWindowElements.getQuantityOfProducts().get(numberOfProduct - 1)).hasText(quantityOfProducts);
        cartAsDropDownMenuModalWindowElements.getLogger().info("End of checking. Amount of products: " + quantityOfProducts);
        return this;
    }

    @Step("Checking name of product in cart. Name of product should be {nameOfProduct}")
    public CartAsDropDownMenuModalWindowActions checkNameOfProductInCart(String nameOfProduct, int numberOfProduct) {
        cartAsDropDownMenuModalWindowElements.getLogger().info("Checking name of product in cart.");
        WaitForElement.waitUntilElementIsVisible(cartAsDropDownMenuModalWindowElements.getNameOfProduct().get(numberOfProduct - 1));
        cartAsDropDownMenuModalWindowElements.getNameOfProduct().get(numberOfProduct - 1).getAttribute("title").equals(nameOfProduct);
        cartAsDropDownMenuModalWindowElements.getLogger().info("End of checking. Name of product is: " + nameOfProduct);
        return this;
    }

    @Step("Checking price of product in cart. Price of product should be {productPrice}")
    public CartAsDropDownMenuModalWindowActions checkTotalPriceOfProductInCart(String productPrice, int numberOfProduct) {
        cartAsDropDownMenuModalWindowElements.getLogger().info("Checking price of product in cart.");
        WaitForElement.waitUntilElementIsVisible(cartAsDropDownMenuModalWindowElements.getPriceOfProduct().get(numberOfProduct - 1));
        AssertWebElement.assertThat(cartAsDropDownMenuModalWindowElements.getPriceOfProduct().get(numberOfProduct - 1)).hasText(productPrice);
        cartAsDropDownMenuModalWindowElements.getLogger().info("End of checking. Price of product is: " + productPrice);
        return this;
    }

    @Step("Checking shipping price of product in cart. Shipping price of product should be {shippingPrice}")
    public CartAsDropDownMenuModalWindowActions checkShippingPriceInCart(String shippingPrice) {
        cartAsDropDownMenuModalWindowElements.getLogger().info("Checking shipping price of product in cart.");
        WaitForElement.waitUntilElementIsVisible(cartAsDropDownMenuModalWindowElements.getShippingPrice());
        AssertWebElement.assertThat(cartAsDropDownMenuModalWindowElements.getShippingPrice()).hasText(shippingPrice);
        cartAsDropDownMenuModalWindowElements.getLogger().info("End of checking. Shipping price of product is: " + shippingPrice);
        return this;
    }

    @Step("Checking total price of products with shipping in cart. Price of products with shipping should be {totalPriceOfProductWithShipping}")
    public CartAsDropDownMenuModalWindowActions checkPriceOfProductWithShippingInCart(String totalPriceOfProductWithShipping) {
        cartAsDropDownMenuModalWindowElements.getLogger().info("Checking total price of products with shipping in cart.");
        WaitForElement.waitUntilElementIsVisible(cartAsDropDownMenuModalWindowElements.getTotalPriceWithShipping());
        AssertWebElement.assertThat(cartAsDropDownMenuModalWindowElements.getTotalPriceWithShipping()).hasText(totalPriceOfProductWithShipping);
        cartAsDropDownMenuModalWindowElements.getLogger().info("End of checking. Total price of products with shipping is: " + totalPriceOfProductWithShipping);
        return this;
    }

    @Step("Checking details of product in cart. Details of product should be {productDetail}")
    public CartAsDropDownMenuModalWindowActions checkProductDetailInCart(String productDetail, int numberOfProduct) {
        cartAsDropDownMenuModalWindowElements.getLogger().info("Checking details of product in cart.");
        WaitForElement.waitUntilElementIsVisible(cartAsDropDownMenuModalWindowElements.getProductDetail().get(numberOfProduct - 1));
        AssertWebElement.assertThat(cartAsDropDownMenuModalWindowElements.getProductDetail().get(numberOfProduct - 1)).hasText(productDetail);
        cartAsDropDownMenuModalWindowElements.getLogger().info("End of checking. Details of product are: " + productDetail);
        return this;
    }

}
