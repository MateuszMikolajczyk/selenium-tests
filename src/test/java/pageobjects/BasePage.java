package pageobjects;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

/**
 * Class for initialization web elements and logger object.
 */
public class BasePage {

    protected Logger logger = LogManager.getLogger(this.getClass().getName());

    protected BasePage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    protected Logger getLogger() {
        return logger;
    }

}
