package pageobjects.pages.actions;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.Select;
import pageobjects.pages.elements.CustomerServicePageElements;
import waits.WaitForElement;

public class CustomerServicePageActions {

    private CustomerServicePageElements customerServicePageElements;

    private CustomerServicePageActions() {
        customerServicePageElements = new CustomerServicePageElements();
    }

    public static CustomerServicePageActions getCustomerServicePageActions() {
        return new CustomerServicePageActions();
    }

    @Step("User choose subject heading")
    public CustomerServicePageActions chooseSubjectHeading(String subjectHeading) {
        customerServicePageElements.getLogger().info("Trying to chose Subject Heading");
        Select select = new Select(customerServicePageElements.getSubjectHeadingSelect());
        select.selectByVisibleText(subjectHeading);
        customerServicePageElements.getLogger().info("Subject Heading is choosed");
        return this;
    }

    @Step("User fill in email {email}")
    public CustomerServicePageActions fillInEmailAddressInput(String email) {
        customerServicePageElements.getLogger().info("Trying to fill in Email field");
        WaitForElement.waitUntilElementIsVisible(customerServicePageElements.getEmailAddressInput());
        customerServicePageElements.getEmailAddressInput().sendKeys(email);
        customerServicePageElements.getLogger().info("Email is filled in");
        return this;
    }

    @Step("User fill in order reference {text}")
    public CustomerServicePageActions fillInOrderReferenceInput(String text) {
        customerServicePageElements.getLogger().info("Trying to fill in Order Reference field");
        customerServicePageElements.getOrderReferenceInput().sendKeys(text);
        customerServicePageElements.getLogger().info("Order Reference is filled in");
        return this;
    }

    @Step("User attach file")
    public CustomerServicePageActions attachFile(String directory) {
        customerServicePageElements.getLogger().info("Trying to attach file");
        customerServicePageElements.getFileInput().sendKeys(directory);
        customerServicePageElements.getLogger().info("File is attached");
        return this;
    }

    @Step("User user fill in text field with text {text}")
    public CustomerServicePageActions fillInTextField(String text) {
        customerServicePageElements.getLogger().info("Trying to fill in text area");
        customerServicePageElements.getTextField().sendKeys(text);
        customerServicePageElements.getLogger().info("Text area is filled in");
        return this;
    }

    @Step("User clicks on button Send")
    public CustomerServicePageActions clickOnButtonSend() {
        customerServicePageElements.getLogger().info("Trying to click button Send");
        customerServicePageElements.getButtonSend().click();
        customerServicePageElements.getLogger().info("Button Send is clicked");
        return this;
    }

    @Step("Checking if the error message {warningMessage} is visible")
    public CustomerServicePageActions assertThatContactServiceErrorIsCorrect(String warningMessage) {
        customerServicePageElements.getLogger().info("Checking if warning message " + warningMessage + " is displayed");
        WaitForElement.waitUntilElementIsVisible(customerServicePageElements.getErrorMessage());
        AssertWebElement.assertThat(customerServicePageElements.getErrorMessage()).hasText(warningMessage);
        customerServicePageElements.getLogger().info("Warning message " + warningMessage + " is checked");
        return this;
    }

    @Step("Checking if the success message {warningMessage} is visible")
    public CustomerServicePageActions assertThatContactServiceSuccessMessageIsCorrect(String warningMessage) {
        customerServicePageElements.getLogger().info("Checking if warning message " + warningMessage + " is displayed");
        WaitForElement.waitUntilElementIsVisible(customerServicePageElements.getSuccessMessage());
        AssertWebElement.assertThat(customerServicePageElements.getSuccessMessage()).hasText(warningMessage);
        customerServicePageElements.getLogger().info("Warning message " + warningMessage + " is checked");
        return this;
    }

    @Step("Checking if the heading {heading} is visible")
    public CustomerServicePageActions assertThatHeadingCustomerServiceIsDisplayed(String heading) {
        customerServicePageElements.getLogger().info("Checking if heading " + heading + " is displayed");
        WaitForElement.waitUntilElementIsVisible(customerServicePageElements.getCustomerServiceHeading());
        AssertWebElement.assertThat(customerServicePageElements.getCustomerServiceHeading()).hasText(heading);
        customerServicePageElements.getLogger().info("Heading " + heading + " is checked");
        return this;
    }

}
