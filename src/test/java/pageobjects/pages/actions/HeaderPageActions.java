package pageobjects.pages.actions;

import io.qameta.allure.Step;
import pageobjects.modalswindows.actions.CartAsDropDownMenuModalWindowActions;
import pageobjects.pages.elements.HeaderPageElements;
import utils.ActionsForCursorAndKeyboard;
import utils.JavascriptForTests;
import waits.WaitForElement;

import static pageobjects.modalswindows.actions.CartAsDropDownMenuModalWindowActions.getCartAsDropDownMenuModalWindowActions;
import static pageobjects.pages.actions.CustomerServicePageActions.getCustomerServicePageActions;
import static pageobjects.pages.actions.LoginPageActions.getLoginPageActions;

public class HeaderPageActions {

    private HeaderPageElements headerPageElements;
    private JavascriptForTests javascriptForTests;
    private ActionsForCursorAndKeyboard actionsForCursorAndKeyboard;

    private HeaderPageActions() {
        headerPageElements = new HeaderPageElements();
        javascriptForTests = new JavascriptForTests();
        actionsForCursorAndKeyboard = new ActionsForCursorAndKeyboard();
    }

    public static HeaderPageActions getHeaderPageActions() {
        return new HeaderPageActions();
    }

    @Step("User clicks on Sing In button")
    public LoginPageActions clickOnSingInButton() {
        headerPageElements.getLogger().info("Trying to click button Sing In");
        WaitForElement.waitUntilElementIsVisible(headerPageElements.getSingInButton());
        headerPageElements.getSingInButton().click();
        headerPageElements.getLogger().info("Sing In button is clicked");
        return getLoginPageActions();
    }

    @Step("User clicks on Contact button")
    public CustomerServicePageActions clickOnContactButton() {
        headerPageElements.getLogger().info("Trying to click button Contact");
        WaitForElement.waitUntilElementIsVisible(headerPageElements.getContactButton());
        headerPageElements.getContactButton().click();
        headerPageElements.getLogger().info("Contact button is clicked");
        return getCustomerServicePageActions();
    }

    @Step("User clicks on Sing Out button")
    public HeaderPageActions clickOnSingOutButton() {
        headerPageElements.getLogger().info("Trying to click button Sing Out");
        WaitForElement.waitUntilElementIsVisible(headerPageElements.getLogOutButton());
        headerPageElements.getLogOutButton().click();
        headerPageElements.getLogger().info("Sing Out button is clicked");
        return this;
    }

    @Step("User hover cursor on button 'Cart'")
    public CartAsDropDownMenuModalWindowActions hoverCursorOnButtonCart() {
        headerPageElements.getLogger().info("Trying to hover cursor on button 'Cart'");
        javascriptForTests.scrollToElement(headerPageElements.getCartButton());
        WaitForElement.waitUntilElementIsVisible(headerPageElements.getCartButton());
        actionsForCursorAndKeyboard.moveCursorToElement(headerPageElements.getCartButton());
        headerPageElements.getLogger().info("Button 'Cart' is hovered");
        return getCartAsDropDownMenuModalWindowActions();
    }

}
