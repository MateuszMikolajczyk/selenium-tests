package pageobjects.pages.actions;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import pageobjects.pages.elements.ForgotYourPasswordPageElements;
import waits.WaitForElement;

public class ForgotYourPasswordPageActions {

    private ForgotYourPasswordPageElements forgotYourPasswordPageElements;

    private ForgotYourPasswordPageActions() {
        forgotYourPasswordPageElements = new ForgotYourPasswordPageElements();
    }

    public static ForgotYourPasswordPageActions getForgotYourPasswordPageActions() {
        return new ForgotYourPasswordPageActions();
    }

    @Step("Checking if the heading {heading} is visible")
    public ForgotYourPasswordPageActions assertThatInfoAccountIsDisplayed(String heading) {
        forgotYourPasswordPageElements.getLogger().info("Checking if heading " + heading + " is displayed");
        WaitForElement.waitUntilElementIsVisible(forgotYourPasswordPageElements.getForgotPasswordHeading());
        AssertWebElement.assertThat(forgotYourPasswordPageElements.getForgotPasswordHeading()).hasText(heading);
        forgotYourPasswordPageElements.getLogger().info("Heading " + heading + " is checked");
        return this;
    }

}
