package pageobjects.pages.actions;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import pageobjects.pages.elements.LoginPageElements;
import waits.WaitForElement;

public class LoginPageActions {

    private LoginPageElements loginPageElements;

    private LoginPageActions() {
        loginPageElements = new LoginPageElements();
    }

    public static LoginPageActions getLoginPageActions() {
        return new LoginPageActions();
    }

    @Step("User fill in email {email}")
    public LoginPageActions fillInEmailInput(String email) {
        loginPageElements.getLogger().info("Trying to fill in Email field");
        WaitForElement.waitUntilElementIsVisible(loginPageElements.getEmailInput());
        inputActions(loginPageElements.getEmailInput(), email);
        loginPageElements.getLogger().info("Email is filled in");
        return this;
    }

    @Step("User fill in password {password}")
    public LoginPageActions fillInPasswordInput(String password) {
        loginPageElements.getLogger().info("Trying to fill in Password field");
        inputActions(loginPageElements.getPasswordInput(), password);
        loginPageElements.getLogger().info("Password is filled in");
        return this;
    }

    @Step("User clicks on Sing In button")
    public LoginPageActions clickOnButtonSingIn() {
        loginPageElements.getLogger().info("Trying to click button Sing In");
        loginPageElements.getSingInButton().click();
        loginPageElements.getLogger().info("Sing In button is clicked");
        return this;
    }

    @Step("User clicks on Forget Password link")
    public ForgotYourPasswordPageActions clickOnLinkForgetPassword() {
        loginPageElements.getLogger().info("Trying to click link Forget Password");
        loginPageElements.getForgetPasswordLink().click();
        loginPageElements.getLogger().info("Forget Password link is clicked");
        return ForgotYourPasswordPageActions.getForgotYourPasswordPageActions();
    }

    private void inputActions(WebElement element, String data) {
        element.click();
        element.sendKeys(data);
    }

    @Step("Checking if the error message {warningMessage} is visible")
    public LoginPageActions assertThatLoginErrorIsDisplayed(String warningMessage) {
        loginPageElements.getLogger().info("Checking if warning message " + warningMessage + " is displayed");
        AssertWebElement.assertThat(loginPageElements.getLoginError()).hasText(warningMessage);
        loginPageElements.getLogger().info("Warning message " + warningMessage + " is checked");
        return this;
    }

    @Step("Checking if the heading {text} is visible")
    public LoginPageActions assertThatHeadingLoginPageIsDisplayed(String text) {
        loginPageElements.getLogger().info("Checking if heading " + text + " is displayed");
        AssertWebElement.assertThat(loginPageElements.getMainLoginPageHeading()).hasText(text);
        loginPageElements.getLogger().info("Heading " + text + " is checked");
        return this;
    }

    @Step("Checking if the headings {text1}, {text2}, {text3} are visible")
    public LoginPageActions assertThatHeadingsOnLoginPageAreDisplayed(String text1, String text2, String text3) {
        loginPageElements.getLogger().info("Checking if headings " + text1 + ", " + text2 + ", " + text3 + "," + " are displayed");
        WaitForElement.waitUntilElementIsVisible(loginPageElements.getMainLoginPageHeading());
        AssertWebElement.assertThat(loginPageElements.getMainLoginPageHeading()).hasText(text1);
        AssertWebElement.assertThat(loginPageElements.getCreateAnAccountSubheading()).hasText(text2);
        AssertWebElement.assertThat(loginPageElements.getAlreadyRegisteredSubheading()).hasText(text3);
        loginPageElements.getLogger().info("Headings " + text1 + ", " + text2 + ", " + text3 + "," + " are checked");
        return this;
    }

}
