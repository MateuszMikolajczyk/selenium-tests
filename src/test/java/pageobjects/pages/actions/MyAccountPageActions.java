package pageobjects.pages.actions;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import pageobjects.pages.elements.MyAccountPageElements;
import waits.WaitForElement;

public class MyAccountPageActions {

    private MyAccountPageElements myAccountPageElements;

    private MyAccountPageActions() {
        myAccountPageElements = new MyAccountPageElements();
    }

    public static MyAccountPageActions getMyAccountPageActions() {
        return new MyAccountPageActions();
    }

    @Step("Checking if the message {message} is visible")
    public MyAccountPageActions assertThatInfoAccountIsDisplayed(String message) {
        myAccountPageElements.getLogger().info("Checking if message " + message + " is displayed");
        WaitForElement.waitUntilElementIsVisible(myAccountPageElements.getInfoAccountMessage());
        AssertWebElement.assertThat(myAccountPageElements.getInfoAccountMessage()).hasText(message);
        myAccountPageElements.getLogger().info("Message " + message + " is checked");
        return this;
    }

}
