package pageobjects.pages.actions;

import io.qameta.allure.Step;
import pageobjects.modalswindows.actions.AddToCartModalWindowActions;
import pageobjects.pages.elements.HomePageElements;
import utils.ActionsForCursorAndKeyboard;
import utils.JavascriptForTests;
import waits.WaitForElement;

public class HomePageActions {

    private HomePageElements homePageElements;
    private JavascriptForTests javascriptForTests;
    private ActionsForCursorAndKeyboard actionsForCursorAndKeyboard;

    private HomePageActions() {
        homePageElements = new HomePageElements();
        javascriptForTests = new JavascriptForTests();
        actionsForCursorAndKeyboard = new ActionsForCursorAndKeyboard();
    }

    public static HomePageActions getHomePageActions() {
        return new HomePageActions();
    }

    @Step("User set popular products tab on home page")
    public HomePageActions setProductsTab(String productsTab) {
        homePageElements.getLogger().info("Trying to set " + productsTab + " tab");
        javascriptForTests.scrollToElement(homePageElements.getPopularProductsTab());
        if (productsTab.equals("POPULAR")) {
            homePageElements.getPopularProductsTab().click();
        }else if (productsTab.equals("BEST SELLERS")){
            homePageElements.getBestSellersProductsTab().click();
        }
        homePageElements.getLogger().info( productsTab + " tab is set");
        return this;
    }

    @Step("User hover cursor on popular product from popular product list")
    public HomePageActions hoverCursorOnPopularProduct(int numberOfProduct) {
        homePageElements.getLogger().info("Trying to hover cursor on popular product");
        javascriptForTests.scrollToElement(homePageElements.getPopularProductsImages().get(numberOfProduct - 1));
        WaitForElement.waitUntilElementIsVisible(homePageElements.getPopularProductsImages().get(numberOfProduct - 1));
        actionsForCursorAndKeyboard.moveCursorToElement(homePageElements.getPopularProductsImages().get(numberOfProduct - 1));
        homePageElements.getLogger().info("Product is hovered");
        return this;
    }

    @Step("User hover cursor on best seller product from best sellers products list")
    public HomePageActions hoverCursorOnBestSellersProducts(int numberOfProduct) {
        homePageElements.getLogger().info("Trying to hover cursor on best seller product");
        javascriptForTests.scrollToElement(homePageElements.getBestSellersProductsImages().get(numberOfProduct - 1));
        WaitForElement.waitUntilElementIsVisible(homePageElements.getBestSellersProductsImages().get(numberOfProduct - 1));
        actionsForCursorAndKeyboard.moveCursorToElement(homePageElements.getBestSellersProductsImages().get(numberOfProduct - 1));
        homePageElements.getLogger().info("Product is hovered");
        return this;
    }

    @Step("User clicks button Add to cart from popular products list")
    public AddToCartModalWindowActions clickButtonAddToCartFromPopularProductsList(int numberOfProduct) {
        homePageElements.getLogger().info("Trying to click button Add to cart");
        homePageElements.getAllAddToCartButtonsFromPopularProducts().get(numberOfProduct - 1).click();
        homePageElements.getLogger().info("Add to cart button is clicked");
        return AddToCartModalWindowActions.getAddToCartModalWindowActions();
    }

    @Step("User clicks button Add to cart from best sellers products list")
    public AddToCartModalWindowActions clickButtonAddToCartFromBestSellersProductsList(int numberOfProduct) {
        homePageElements.getLogger().info("Trying to click button Add to cart");
        homePageElements.getAllAddToCartButtonsFromBestSellersProducts().get(numberOfProduct - 1).click();
        homePageElements.getLogger().info("Add to cart button is clicked");
        return AddToCartModalWindowActions.getAddToCartModalWindowActions();
    }

}
