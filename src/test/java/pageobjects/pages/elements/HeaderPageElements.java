package pageobjects.pages.elements;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

public class HeaderPageElements extends BasePage {

    @FindBy(className = "login")
    private WebElement singInButton;

    @FindBy(className = "logout")
    private WebElement logOutButton;

    @FindBy(id = "contact-link")
    private WebElement contactButton;

    @FindBy(xpath = "//div[@class=\'shopping_cart\']/a")
    private WebElement cartButton;

    public WebElement getSingInButton() {
        return singInButton;
    }

    public WebElement getLogOutButton() {
        return logOutButton;
    }

    public WebElement getContactButton() {
        return contactButton;
    }

    public WebElement getCartButton() {
        return cartButton;
    }

    public Logger getLogger() {
        return logger;
    }

}
