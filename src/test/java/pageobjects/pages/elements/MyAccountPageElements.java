package pageobjects.pages.elements;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

public class MyAccountPageElements extends BasePage {

    @FindBy(className = "info-account")
    private WebElement infoAccountMessage;

    public WebElement getInfoAccountMessage() {
        return infoAccountMessage;
    }

    public Logger getLogger() {
        return logger;
    }
}
