package pageobjects.pages.elements;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

public class LoginPageElements extends BasePage {

    @FindBy(name = "email")
    private WebElement emailInput;

    @FindBy(name = "passwd")
    private WebElement passwordInput;

    @FindBy(id = "SubmitLogin")
    private WebElement singInButton;

    @FindBy(xpath = "//div[@class='alert alert-danger']/ol/li")
    private WebElement loginError;

    @FindBy(className = "page-heading")
    private WebElement mainLoginPageHeading;

    @FindBy(xpath = "(//h3[@class='page-subheading'])[1]")
    private WebElement createAnAccountSubheading;

    @FindBy(xpath = "(//h3[@class='page-subheading'])[2]")
    private WebElement alreadyRegisteredSubheading;

    @FindBy(xpath = "//p[@class='lost_password form-group']/a")
    private WebElement forgetPasswordLink;

    public WebElement getEmailInput() {
        return emailInput;
    }

    public WebElement getPasswordInput() {
        return passwordInput;
    }

    public WebElement getSingInButton() {
        return singInButton;
    }

    public WebElement getLoginError() {
        return loginError;
    }

    public WebElement getMainLoginPageHeading() {
        return mainLoginPageHeading;
    }

    public WebElement getCreateAnAccountSubheading() {
        return createAnAccountSubheading;
    }

    public WebElement getAlreadyRegisteredSubheading() {
        return alreadyRegisteredSubheading;
    }

    public WebElement getForgetPasswordLink() {
        return forgetPasswordLink;
    }

    public Logger getLogger() {
        return logger;
    }

}
