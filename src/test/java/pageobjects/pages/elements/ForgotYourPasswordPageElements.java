package pageobjects.pages.elements;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

public class ForgotYourPasswordPageElements extends BasePage {

    @FindBy(className = "page-subheading")
    private WebElement forgotPasswordHeading;

    public WebElement getForgotPasswordHeading() {
        return forgotPasswordHeading;
    }

    public Logger getLogger() {
        return logger;
    }
}
