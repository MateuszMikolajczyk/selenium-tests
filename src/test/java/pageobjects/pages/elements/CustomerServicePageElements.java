package pageobjects.pages.elements;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

public class CustomerServicePageElements extends BasePage {

    @FindBy(xpath = "//select[@id='id_contact']")
    private WebElement subjectHeadingSelect;

    @FindBy(id = "email")
    private WebElement emailAddressInput;

    @FindBy(id = "id_order")
    private WebElement orderReferenceInput;

    @FindBy(id = "fileUpload")
    private WebElement fileInput;

    @FindBy(id = "message")
    private WebElement textField;

    @FindBy(id = "submitMessage")
    private WebElement buttonSend;

    @FindBy(xpath = "//div[@id='center_column']/div/ol/li")
    private WebElement errorMessage;

    @FindBy(xpath = "//div[@id='center_column']/p")
    private WebElement successMessage;

    @FindBy(xpath = "//div[@id='center_column']/h1")
    private WebElement customerServiceHeading;

    public WebElement getSubjectHeadingSelect() {
        return subjectHeadingSelect;
    }

    public WebElement getEmailAddressInput() {
        return emailAddressInput;
    }

    public WebElement getOrderReferenceInput() {
        return orderReferenceInput;
    }

    public WebElement getFileInput() {
        return fileInput;
    }

    public WebElement getTextField() {
        return textField;
    }

    public WebElement getButtonSend() {
        return buttonSend;
    }

    public WebElement getErrorMessage() {
        return errorMessage;
    }

    public WebElement getSuccessMessage() {
        return successMessage;
    }

    public WebElement getCustomerServiceHeading() {
        return customerServiceHeading;
    }

    public Logger getLogger() {
        return logger;
    }

}
