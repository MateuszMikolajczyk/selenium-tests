package pageobjects.pages.elements;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

import java.util.List;

public class HomePageElements extends BasePage {

    @FindBy(xpath = "//a[@class='homefeatured']")
    private WebElement popularProductsTab;

    @FindBy(xpath = "//a[@class='blockbestsellers']")
    private WebElement bestSellersProductsTab;

    @FindBy(xpath = "//ul[@id='homefeatured']//a[@class='button ajax_add_to_cart_button btn btn-default']")
    private List<WebElement> allAddToCartButtonsFromPopularProducts;

    @FindBy(xpath = "//ul[@id='blockbestsellers']//a[@class='button ajax_add_to_cart_button btn btn-default']")
    private List<WebElement> allAddToCartButtonsFromBestSellersProducts;

    @FindBy(xpath = "//ul[@id=\"homefeatured\"]//img")
    private List<WebElement> popularProductsImages;

    @FindBy(xpath = "//ul[@id=\"blockbestsellers\"]//img")
    private List<WebElement> bestSellersProductsImages;

    public WebElement getPopularProductsTab() {
        return popularProductsTab;
    }

    public WebElement getBestSellersProductsTab() {
        return bestSellersProductsTab;
    }

    public List<WebElement> getAllAddToCartButtonsFromPopularProducts() {
        return allAddToCartButtonsFromPopularProducts;
    }

    public List<WebElement> getAllAddToCartButtonsFromBestSellersProducts() {
        return allAddToCartButtonsFromBestSellersProducts;
    }

    public List<WebElement> getPopularProductsImages() {
        return popularProductsImages;
    }

    public List<WebElement> getBestSellersProductsImages() {
        return bestSellersProductsImages;
    }

    public Logger getLogger() {
        return logger;
    }

}
