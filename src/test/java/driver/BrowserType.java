package driver;

/**
 * Enum with defined browsers types.
 */
public enum BrowserType {

    CHROME("chrome"),
    FIREFOX("firefox"),
    IE("internetexplorel"),
    OPERA("opera");

    private final String browser;

    BrowserType(String browser) {
        this.browser = browser;
    }

}
