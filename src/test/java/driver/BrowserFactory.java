package driver;

import configuration.LocalWebDriverProperties;
import configuration.TestRunProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class where are set drivers for particular browsers.
 */
public class BrowserFactory {

    private static DesiredCapabilities desiredCapabilities;
    private static ChromeOptions chromeOptions;
    private static FirefoxOptions firefoxOptions;
    private static InternetExplorerOptions internetExplorerOptions;
    private static OperaOptions operaOptions;

    private boolean isRemoteRun;
    private BrowserType browserType;

    public BrowserFactory(BrowserType browserType, boolean isRemoteRun) {
        this.isRemoteRun = isRemoteRun;
        this.browserType = browserType;
    }

    /**
     * Method for creation web driver object for particular browsers. You can create web driver object for selenium grid or for local web drivers.
     * @return web driver object.
     */
    public WebDriver getBrowser() {
        //Selenium grid.
        if (isRemoteRun) {
            desiredCapabilities = new DesiredCapabilities();
            switch (browserType) {
                case CHROME:
                    chromeOptions = new ChromeOptions();
                    desiredCapabilities.merge(chromeOptions);
                    return getRemoteWebDriver(desiredCapabilities);
                case FIREFOX:
                    firefoxOptions = new FirefoxOptions();
                    desiredCapabilities.merge(firefoxOptions);
                    return getRemoteWebDriver(desiredCapabilities);
                case IE:
                    internetExplorerOptions = new InternetExplorerOptions();
                    desiredCapabilities.merge(internetExplorerOptions);
                    return getRemoteWebDriver(desiredCapabilities);
                case OPERA:
                    operaOptions = new OperaOptions();
                    desiredCapabilities.merge(operaOptions);
                    return getRemoteWebDriver(desiredCapabilities);
                default:
                    throw new IllegalStateException("Unknown browser type!!!");
            }
        //Local web drivers.
        } else {
            desiredCapabilities = new DesiredCapabilities();
            switch (browserType) {
                case CHROME:
                    System.setProperty("webdriver.chrome.driver", LocalWebDriverProperties.getChromeDriverLocation());
                    return new ChromeDriver();
                case FIREFOX:
                    System.setProperty("webdriver.gecko.driver", LocalWebDriverProperties.getFirefoxDriverLocation());
                    return new FirefoxDriver();
                case IE:
                    System.setProperty("webdriver.ie.driver", LocalWebDriverProperties.getIeDriverLocation());
                    return new InternetExplorerDriver();
                case OPERA:
                    System.setProperty("webdriver.opera.driver", LocalWebDriverProperties.getOperaDriverLocation());
                    return new OperaDriver();
                default:
                    throw new IllegalStateException("Unknown browser type!!!");
            }
        }
    }

    /**
     * Private method for creation remote web driver for selenium grid.
     * @param desiredCapabilities capabilities for web browsers.
     * @return remote web driver object.
     */
    private WebDriver getRemoteWebDriver(DesiredCapabilities desiredCapabilities) {
        RemoteWebDriver remoteWebDriver = null;

        try {
            remoteWebDriver = new RemoteWebDriver(new URL(TestRunProperties.getGridUrl()), desiredCapabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to create RemoteWebDriver due to: " + e.getMessage());
        }
        return remoteWebDriver;
    }

}
