package driver.listeners;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/**
 * Class with static method for register event listener.
 */
public class WebDriverEventListenerRegistrar {

    /**
     * Static method for register event listener.
     * @param driver WebDriver object.
     * @return webDriver object.
     */
    public synchronized static WebDriver registerWebDriverEventListener(WebDriver driver) {
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        DriverEventListener driverEventListener = new DriverEventListener();
        return eventFiringWebDriver.register(driverEventListener);
    }

}
