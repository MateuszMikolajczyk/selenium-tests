package driver.manager;

import driver.BrowserFactory;
import driver.listeners.WebDriverEventListenerRegistrar;
import org.openqa.selenium.WebDriver;

import static configuration.TestRunProperties.getBrowserToRun;
import static configuration.TestRunProperties.getIsRemoteRun;
import static driver.BrowserType.FIREFOX;

/**
 * Class based on Singleton pattern. This one is for managing driver.
 */
public class DriverManager {

    private static ThreadLocal<WebDriver> webDriverThreadLocal = new ThreadLocal<>();

    private DriverManager() {
    }

    /**
     * Static method for get driver instance.
     */
    public static WebDriver getWebDriver() {

        if (webDriverThreadLocal.get() == null) {
            webDriverThreadLocal.set(WebDriverEventListenerRegistrar.registerWebDriverEventListener(new BrowserFactory(getBrowserToRun(), getIsRemoteRun()).getBrowser()));
        }

        return webDriverThreadLocal.get();
    }

    /**
     * Static method for close web browser and kill driver instance.
     */
    public static void disposeDriver() {
        webDriverThreadLocal.get().close();
        if (!getBrowserToRun().equals(FIREFOX)) {
            webDriverThreadLocal.get().quit();
        }
        webDriverThreadLocal.remove();
    }

}
