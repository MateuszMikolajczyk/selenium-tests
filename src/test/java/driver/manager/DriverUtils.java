package driver.manager;

import io.qameta.allure.Step;

/**
 * Class with base configuration for web browsers.
 */
public class DriverUtils {

    /**
     * Static method for set initial configuration for browser. For now in this method you can find maximize browser window action.
     */
    public static void setInitialConfiguration() {
        DriverManager.getWebDriver().manage().window().maximize();
    }

    /**
     * Static method for navigate to target web page.
     * @param pageUrl url of web page.
     */
    @Step("User go to {pageUrl}")
    public static void navigateToPage(String pageUrl) {
        DriverManager.getWebDriver().navigate().to(pageUrl);
    }

}
