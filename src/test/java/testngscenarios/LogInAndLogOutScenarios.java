package testngscenarios;

import driver.manager.DriverUtils;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;
import pageobjects.pages.actions.HeaderPageActions;
import pageobjects.pages.actions.LoginPageActions;
import pageobjects.pages.actions.MyAccountPageActions;

import static navigation.ApplicationUrls.MAIN_URL;
import static pageobjects.pages.actions.HeaderPageActions.getHeaderPageActions;
import static pageobjects.pages.actions.LoginPageActions.getLoginPageActions;
import static pageobjects.pages.actions.MyAccountPageActions.getMyAccountPageActions;

public class LogInAndLogOutScenarios {

    private HeaderPageActions headerPageActions;
    private MyAccountPageActions myAccountPageActions;
    private LoginPageActions loginPageActions;

    private String email = "testautomatt@gmail.com";
    private String password = "ADMIN";
    private String accountWelcomingInformation = "Welcome to your account. Here you can manage all of your personal information and orders.";
    private String loginPageHeading = "AUTHENTICATION";

    @Test(description = "As user log in to application and log out.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is to log in using proper username and password" +
            "and check if warning message \"Welcome to your account. Here you can manage all of your personal information and orders.\" is displayed")
    public void asUserLogInToApplicationAndLogOut() {

        headerPageActions = getHeaderPageActions();
        myAccountPageActions = getMyAccountPageActions();
        loginPageActions = getLoginPageActions();

        DriverUtils.navigateToPage(MAIN_URL);
        headerPageActions.clickOnSingInButton()
                .fillInEmailInput(email)
                .fillInPasswordInput(password)
                .clickOnButtonSingIn();
        myAccountPageActions.assertThatInfoAccountIsDisplayed(accountWelcomingInformation);
        headerPageActions.clickOnSingOutButton();
        loginPageActions.assertThatHeadingLoginPageIsDisplayed(loginPageHeading);
    }

}
