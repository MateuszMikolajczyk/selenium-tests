package testngscenarios;

import driver.manager.DriverUtils;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.pages.actions.HeaderPageActions;
import pageobjects.pages.actions.LoginPageActions;
import pageobjects.pages.actions.MyAccountPageActions;

import static navigation.ApplicationUrls.MAIN_URL;
import static pageobjects.pages.actions.HeaderPageActions.getHeaderPageActions;
import static pageobjects.pages.actions.LoginPageActions.getLoginPageActions;
import static pageobjects.pages.actions.MyAccountPageActions.getMyAccountPageActions;

public class FailScenariosForScreenshotMechanism {

    private HeaderPageActions headerPageActions;
    private MyAccountPageActions myAccountPageActions;
    private LoginPageActions loginPageActions;

    private String email = "ttestautomatt@gmail.com";
    private String wrongEmail = "ttestautomatt@gmail.com";
    private String password = "ADMIN";
    private String wrongPassword = "ADMINN";
    private String accountWelcomingInformation = "WWelcome to your account. Here you can manage all of your personal information and orders.";
    private String loginPageHeading = "AUTHENTICATION";

    @Test(description = "As user log in to application and log out.", dataProvider = "dataSetForTest")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is to log in using proper username and password" +
            "and check if warning message \"Welcome to your account. Here you can manage all of your personal information and orders.\" is displayed")
    public void asUserLogInToApplicationAndLogOut(String emailData, String passwordData) {

        headerPageActions = getHeaderPageActions();
        myAccountPageActions = getMyAccountPageActions();
        loginPageActions = getLoginPageActions();

        DriverUtils.navigateToPage(MAIN_URL);
        headerPageActions.clickOnSingInButton()
                .fillInEmailInput(emailData)
                .fillInPasswordInput(passwordData)
                .clickOnButtonSingIn();
        myAccountPageActions.assertThatInfoAccountIsDisplayed(accountWelcomingInformation);
        headerPageActions.clickOnSingOutButton();
        loginPageActions.assertThatHeadingLoginPageIsDisplayed(loginPageHeading);
    }

    @DataProvider(name = "dataSetForTest")
    private Object[][] queriesTestData() {
        Object[][] testDataArray = {
                {wrongEmail, password},
                {email, wrongPassword}
        };
        return testDataArray;
    }
}