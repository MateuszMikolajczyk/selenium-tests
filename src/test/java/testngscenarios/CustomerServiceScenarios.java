package testngscenarios;

import driver.manager.DriverUtils;
import io.qameta.allure.Description;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;
import pageobjects.pages.actions.HeaderPageActions;

import java.io.File;

import static navigation.ApplicationUrls.MAIN_URL;
import static pageobjects.pages.actions.HeaderPageActions.getHeaderPageActions;


public class CustomerServiceScenarios {

    private static String subjectHeadingNr1 = "Customer service";
    private static String orderReferenceNr1 = "123";
    private static String textNr1 = "Test text nr1";
    private static String subjectHeadingNr2 = "Webmaster";
    private static String orderReferenceNr2 = "321";
    private static String textNr2 = "Test text nr2";
    private HeaderPageActions headerPageActions;
    private String email = "testautomatt@gmail.com";
    private String filePath = "src/test/resources/TestFile.txt";
    private String contactServiceSuccessMessage = "Your message has been successfully sent to our team.";
    private String subjectHeadingData;
    private String orderReferenceData;
    private String textData;

    private CustomerServiceScenarios(String subjectHeadingData, String orderReferenceData, String textData) {
        this.subjectHeadingData = subjectHeadingData;
        this.orderReferenceData = orderReferenceData;
        this.textData = textData;
    }

    @Factory
    public static Object[] getDataForTests() {
        CustomerServiceScenarios firstDataSet = new CustomerServiceScenarios(subjectHeadingNr1, orderReferenceNr1, textNr1);
        CustomerServiceScenarios secondDataSet = new CustomerServiceScenarios(subjectHeadingNr2, orderReferenceNr2, textNr2);
        return new Object[]{
                firstDataSet,
                secondDataSet
        };
    }

    @Test(description = "As user go to customer service site and send message.")
    @Description("The goal of this test is send message to customer service")
    public void asUserGoToCustomerServiceSiteAndSendMessage() {

        headerPageActions = getHeaderPageActions();

        DriverUtils.navigateToPage(MAIN_URL);
        headerPageActions.clickOnContactButton()
                .fillInEmailAddressInput(email)
                .chooseSubjectHeading(subjectHeadingData)
                .fillInOrderReferenceInput(orderReferenceData)
                .attachFile(new File(filePath).getAbsolutePath())
                .fillInTextField(textData)
                .clickOnButtonSend()
                .assertThatContactServiceSuccessMessageIsCorrect(contactServiceSuccessMessage);
    }
}
