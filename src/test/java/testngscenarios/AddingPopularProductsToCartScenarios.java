package testngscenarios;

import driver.manager.DriverUtils;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;
import pageobjects.modalswindows.actions.AddToCartModalWindowActions;
import pageobjects.pages.actions.HeaderPageActions;
import pageobjects.pages.actions.HomePageActions;

import static navigation.ApplicationUrls.MAIN_URL;
import static pageobjects.modalswindows.actions.AddToCartModalWindowActions.getAddToCartModalWindowActions;
import static pageobjects.pages.actions.HeaderPageActions.getHeaderPageActions;
import static pageobjects.pages.actions.HomePageActions.getHomePageActions;


public class AddingPopularProductsToCartScenarios {

    private HomePageActions homePageActions;
    private AddToCartModalWindowActions addToCartModalWindowActions;
    private HeaderPageActions headerPageActions;

    private String nameOfProductInCart;
    private String detailsOfProductInCart;
    private String priceOfProductInCart;
    private String priceOfShipping;
    private String quantityOfProductInCart;
    private String totalPriceOfProductWithSippingInCart;
    private String informationThatProductWasSuccessfullyAddedToCartPattern = "Product successfully added to your shopping cart";
    private String informationHowManyAreProductsInCartPattern;
    private int numberOfProduct;

    @Test(description = "As user add to cart first popular product and check this one in cart.")
    @Severity(SeverityLevel.NORMAL)
    @Description("The goal of this test is checking if first product on list 'popular products' is adding to cart correctly.")
    public void asUserAddToCartFirstPopularProductAndCheckThisOneInCart() {

        homePageActions = getHomePageActions();
        headerPageActions = getHeaderPageActions();
        addToCartModalWindowActions = getAddToCartModalWindowActions();
        numberOfProduct = 1;

        DriverUtils.navigateToPage(MAIN_URL);
        homePageActions.setProductsTab("POPULAR")
                .hoverCursorOnPopularProduct(numberOfProduct)
                .clickButtonAddToCartFromPopularProductsList(numberOfProduct);

        gatherInformationAboutProduct();
        setCorrectInformationHowManyAreProductsInCartPattern();

        addToCartModalWindowActions.checkInformationAboutThatHowManyProductsAreInCart(informationHowManyAreProductsInCartPattern)
                .checkInformationAboutThatProductWasAddedToCart(informationThatProductWasSuccessfullyAddedToCartPattern)
                .clickButtonContinueShopping();
        headerPageActions.hoverCursorOnButtonCart()
                .checkNameOfProductInCart(nameOfProductInCart, numberOfProduct)
                .checkQuantityOfProductsInCart(quantityOfProductInCart, numberOfProduct)
                .checkProductDetailInCart(detailsOfProductInCart, numberOfProduct)
                .checkTotalPriceOfProductInCart(priceOfProductInCart, numberOfProduct)
                .checkShippingPriceInCart(priceOfShipping)
                .checkPriceOfProductWithShippingInCart(totalPriceOfProductWithSippingInCart);
    }

    @Test(description = "As user add to cart all visible popular products and check these ones in cart.")
    @Severity(SeverityLevel.NORMAL)
    @Description("The goal of this test is checking if all visible products on list 'popular products' are adding to cart correctly.")
    public void asUserAddToCartAllVisiblePopularProductsAndCheckTheseOnesInCart() {

        homePageActions = getHomePageActions();
        headerPageActions = getHeaderPageActions();
        addToCartModalWindowActions = getAddToCartModalWindowActions();

        DriverUtils.navigateToPage(MAIN_URL);

        for (numberOfProduct = 1; numberOfProduct < 8; numberOfProduct++) {
            homePageActions.setProductsTab("POPULAR")
                    .hoverCursorOnPopularProduct(numberOfProduct)
                    .clickButtonAddToCartFromPopularProductsList(numberOfProduct);

            gatherInformationAboutProduct();
            setCorrectInformationHowManyAreProductsInCartPattern();

            addToCartModalWindowActions.checkInformationAboutThatHowManyProductsAreInCart(informationHowManyAreProductsInCartPattern)
                    .checkInformationAboutThatProductWasAddedToCart(informationThatProductWasSuccessfullyAddedToCartPattern)
                    .clickButtonContinueShopping();
            headerPageActions.hoverCursorOnButtonCart()
                    .checkNameOfProductInCart(nameOfProductInCart, numberOfProduct)
                    .checkQuantityOfProductsInCart(quantityOfProductInCart, numberOfProduct)
                    .checkProductDetailInCart(detailsOfProductInCart, numberOfProduct)
                    .checkTotalPriceOfProductInCart(priceOfProductInCart, numberOfProduct)
                    .checkShippingPriceInCart(priceOfShipping)
                    .checkPriceOfProductWithShippingInCart(totalPriceOfProductWithSippingInCart);
        }
    }

    private void gatherInformationAboutProduct() {
        nameOfProductInCart = addToCartModalWindowActions.gatherNameOfProduct();
        detailsOfProductInCart = addToCartModalWindowActions.gatherDetailsOfProduct();
        priceOfProductInCart = addToCartModalWindowActions.gatherPriceOfProduct();
        quantityOfProductInCart = addToCartModalWindowActions.gatherQuantityOfProducts();
        priceOfShipping = addToCartModalWindowActions.gatherShippingPrice();
        totalPriceOfProductWithSippingInCart = addToCartModalWindowActions.gatherTotalPriceWithShipping();
    }

    private void setCorrectInformationHowManyAreProductsInCartPattern() {
        if (numberOfProduct == 1) {
            informationHowManyAreProductsInCartPattern = "There is " + numberOfProduct + " item in your cart.";
        } else {
            informationHowManyAreProductsInCartPattern = "There are " + numberOfProduct + " items in your cart.";
        }
    }
}
