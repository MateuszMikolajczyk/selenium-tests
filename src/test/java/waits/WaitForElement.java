package waits;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Class with waits for web elements.
 */
public class WaitForElement {

    private static WebDriverWait getWebDriverWait(int time) {
        return new WebDriverWait(DriverManager.getWebDriver(), time);
    }

    /**
     * Method with wait for element util it will be visible on page.
     * @param webElement queried web element.
     */
    public static void waitUntilElementIsVisible(WebElement webElement) {
        WebDriverWait webDriverWait = getWebDriverWait(10);
        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
    }

    /**
     * Method with wait for element util it will be clickable on page.
     * @param webElement queried web element.
     */
    public static void waitUntilElementIsClickable(WebElement webElement) {
        WebDriverWait webDriverWait = getWebDriverWait(10);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
    }
}
