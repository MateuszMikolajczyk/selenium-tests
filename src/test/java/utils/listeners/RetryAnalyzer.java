package utils.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

/**
 * Class where is implemented whole mechanism of repeating failed scenarios.
 */
public class RetryAnalyzer implements IRetryAnalyzer {

    private static final int MAX_NUMBER_OF_RETRIES = 2;
    private int count = 0;
    private Logger logger = LogManager.getLogger(RetryAnalyzer.class);

    /**
     * Public method with logic of repeating failed scenarios.
     * Method will return true value if scenario is failed and "count" variable be lower than "MAX_NUMBER_OF_RETRIES".
     * In otherwise method will return false value and scenario will not repeat.
     * @param iTestResult object of ITestResult class.
     * @return boolean value.
     */
    @Override
    public boolean retry(ITestResult iTestResult) {
        if (!iTestResult.isSuccess()) {
            if (count < MAX_NUMBER_OF_RETRIES) {
                count++;
                logger.info("Retrying test method {}!", iTestResult.getName());
                return true;
            }
        }
        logger.info("Test method {} will be not retried!", iTestResult.getName());
        return false;
    }

}
