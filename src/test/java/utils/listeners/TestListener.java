package utils.listeners;

import configuration.ConfigurationProperties;
import configuration.PropertiesLoader;
import driver.manager.DriverManager;
import driver.manager.DriverUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import utils.ScreenShotMaker;

import java.util.Properties;

/**
 * Class with listeners for scenarios configuration and actions.
 */
public class TestListener implements ITestListener {
    private Logger logger = LogManager.getLogger(TestListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        DriverManager.getWebDriver();
        DriverUtils.setInitialConfiguration();
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info("Test {} passed successfully", result.getName());
        DriverManager.disposeDriver();
    }

    @Override
    public void onTestFailure(ITestResult result) {
        logger.info("Test {} failed!", result.getName());
        ScreenShotMaker.makeScreenShot();
        DriverManager.disposeDriver();
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        logger.info("Test {} skipped!", result.getName());
        ScreenShotMaker.makeScreenShot();
        DriverManager.disposeDriver();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        logger.info("Test {} failed!", result.getName());
        ScreenShotMaker.makeScreenShot();
        DriverManager.disposeDriver();
    }

    @Override
    public void onStart(ITestContext context) {
        PropertiesLoader propertiesLoader = new PropertiesLoader();
        Properties propertiesFromFile = propertiesLoader.getPropertiesFromFile("congiguration.properties");
        ConfigurationProperties.setProperties(propertiesFromFile);
    }

    @Override
    public void onFinish(ITestContext context) {
    }
}
