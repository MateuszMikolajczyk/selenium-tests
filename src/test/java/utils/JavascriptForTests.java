package utils;

import driver.manager.DriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

/**
 * Class with java script actions on page during tests.
 */
public class JavascriptForTests {

    private JavascriptExecutor javascriptExecutor = null;

    public JavascriptForTests(){
        javascriptExecutor = (JavascriptExecutor) DriverManager.getWebDriver();
    }

    public void scrollToElement(WebElement element){
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);",element);
    }

}
