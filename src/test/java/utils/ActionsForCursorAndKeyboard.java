package utils;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * Class with actions for mouse and keyboard during tests.
 */
public class ActionsForCursorAndKeyboard {

    private Actions actions = null;

    public ActionsForCursorAndKeyboard() {
        actions = new Actions(DriverManager.getWebDriver());
    }

    public void moveCursorToElement(WebElement webElement) {
        actions.moveToElement(webElement).build().perform();
    }

}
