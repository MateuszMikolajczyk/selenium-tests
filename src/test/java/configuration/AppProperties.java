package configuration;

/**
 * Class for return URLs hardcoded in property file.
 */
public class AppProperties {

    /**
     * Static method returning home page url from property file.
     *
     * @return home page url.
     */
    public static String getMainUrl() {
        return ConfigurationProperties.getProperties().getProperty("main.url");
    }

    /**
     * Static method returning login page url from property file.
     *
     * @return login page url.
     */
    public static String getLoginUrl() {
        return ConfigurationProperties.getProperties().getProperty("login.url");
    }

    /**
     * Static method returning customer service page url from property file.
     *
     * @return customer service page url.
     */
    public static String getServiceCustomerUrl() {
        return ConfigurationProperties.getProperties().getProperty("customer.service.url");
    }

}
