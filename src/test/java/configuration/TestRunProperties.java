package configuration;

import driver.BrowserType;

/**
 * Class for return data from property file like type of browser for run, URL for selenium GRID and flag for remote(selenium GRID) or local run for tests.
 */
public class TestRunProperties {

    /**
     * Static method returning Grid url.
     *
     * @return Grid url.
     */
    public static String getGridUrl() {
        return ConfigurationProperties.getProperties().getProperty("grid.url");
    }

    /**
     * Static method returning type of browser to run for tests.
     *
     * @return type of browser.
     */
    public static BrowserType getBrowserToRun() {
        return BrowserType.valueOf(ConfigurationProperties.getProperties().getProperty("browser"));
    }

    /**
     * Static method returning flag for remote or local run of tests.
     *
     * @return flag for local or remote run of tests.
     */
    public static boolean getIsRemoteRun() {
        return Boolean.parseBoolean(ConfigurationProperties.getProperties().getProperty("is.remote.run"));
    }
}
