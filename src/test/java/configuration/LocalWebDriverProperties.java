package configuration;

/**
 * Class for return locations of web drivers hardcoded in property file.
 */
public class LocalWebDriverProperties {

    /**
     * Static method returning location Chrome driver from property file.
     *
     * @return location Chrome driver.
     */
    public static String getChromeDriverLocation() {
        return ConfigurationProperties.getProperties().getProperty("chrome.driver.location");
    }

    /**
     * Static method returning location Firefox driver from property file.
     *
     * @return location Firefox driver.
     */
    public static String getFirefoxDriverLocation() {
        return ConfigurationProperties.getProperties().getProperty("firefox.driver.location");
    }

    /**
     * Static method returning location IE driver from property file.
     *
     * @return location IE driver.
     */
    public static String getIeDriverLocation() {
        return ConfigurationProperties.getProperties().getProperty("ie.driver.location");
    }

    /**
     * Static method returning location Opera driver from property file.
     *
     * @return location Opera driver.
     */
    public static String getOperaDriverLocation() {
        return ConfigurationProperties.getProperties().getProperty("opera.driver.location");
    }

}
